import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueTimeago from 'vue-timeago'

Vue.config.productionTip = false
Vue.use(VueTimeago, {
  name: 'Timeago', // Component name, `Timeago` by default
  locale: 'en', // Default locale
  // We use `date-fns` under the hood
  // So you can use all locales from it
  locales: {
    'zh-CN': require('date-fns/locale/zh_cn'),
    ja: require('date-fns/locale/ja')
  }
})

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
